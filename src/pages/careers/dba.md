---
title: Senior Database Engineer
requirements:
  - Strong optimization skills of large and heavily loaded databases (>1TiB, >10k QpS).
  - 3+ years of experience running PostgreSQL in large production environments.
  - Advanced SQL and PL/pgSQL knowledge.
---

# Senior Database Engineer

As a Senior Database Engineer, you will be working on state-of-the-art solutions, involving the following topics:

- PostgreSQL SQL optimization
- database and CI/CD
- clouds
- partitioning, sharding
- database monitoring

## Requirements

- Strong optimization skills of large and heavily loaded databases (>1TiB, >10k QpS).
- 3+ years of experience running PostgreSQL in large production environments.
- Advanced SQL and PL/pgSQL knowledge.
- Knowledge of PostgreSQL internals.
- A history of increasing responsibility in the field of databases.
- Experience and skills in the field of systems performance (PostgreSQL and Linux monitoring, troubleshooting, tuning).
- Self-motivation and strong desire to achieve the highest levels of expertise.
- Solid skills of reading and using EXPLAIN command to troubleshoot and optimize SQL queries.

## Nice-to-haves

- Advanced topics (partitioning, queues in database, time series, window functions, denormalization, efficient work with arrays, JSON).
- Experience of implementation of SQL optimization workflow, involving `pg_stat_statements`, log-based analysis, etc.
- Experience of development/tuning of advanced PostgreSQL monitoring setups either from scratch or based on existing components.
- Deep understanding of file systems.
- ZFS experience.
- Go and/or C.
- Python and/or Ruby.
- Oracle or SQL Server optimization experience.
- Knowledge of basic machine learning algorithms and experience/with popular machine learning frameworks.
- Cloud experience (AWS, GCP).
- Experience working in a distributed team.

## Responsibilities

- Development of new open-source to automate:
  - SQL query optimization,
  - database experiments to verify DB migrations, complex changes, etc,
  - PostgreSQL configuration tuning.
- SQL performance troubleshooting and optimization.
- DB schema design to store data securely and efficiently.
- Code reviews and interaction with development teams.
- Setting up SQL optimization workflow in various development teams.
- Direct work with our clients to implement our methodologies and tools.

## Benefits

- Development of game-changing tools for software engineers.
- Interesting and challenging tasks, basis for constant learning of new technologies.
- Team of professionals and a supportive atmosphere.
- Extremely competitive pay depending on experience and skills.
- Flexible working hours/home-office.
- Health care payment.
- Online IT / Business English course.

Send us your CV to join@postgres.ai
