export type Author = {
  avatarUrl: string
  name: string
  gitlabUrl: string
  githubUrl: string
  linkedinUrl?: string
  role?: string
  twitterUrl?: string
  note?: string
}

const authors: { [key: string]: Author } = {
  nik: {
    avatarUrl: '/assets/author/nik.jpg',
    name: 'Nikolay Samokhvalov',
    role: 'CEO & Founder of',
    twitterUrl: 'https://twitter.com/samokhvalov',
    gitlabUrl: 'https://gitlab.com/NikolayS',
    githubUrl: 'https://github.com/NikolayS',
    linkedinUrl: 'https://www.linkedin.com/in/samokhvalov',
    note: 'Working on tools to balance Dev with Ops in DevOps',
  },
  dante: {
    avatarUrl: '/assets/author/dante.jpg',
    name: 'Dante Cassanego',
    twitterUrl: 'https://twitter.com/dantec',
    gitlabUrl: 'https://gitlab.com/dante.cassanego',
    githubUrl: 'https://github.com/dcassanego',
    linkedinUrl: 'https://www.linkedin.com/in/dantecassanego',
  },
  anatoly: {
    avatarUrl: '/assets/author/anatoly.jpg',
    name: 'Anatoly Stansler',
    gitlabUrl: 'https://gitlab.com/anatolystansler',
    githubUrl: 'https://github.com/anatolystansler',
    linkedinUrl: 'https://www.linkedin.com/in/anatoly-stansler-37265514a',
  },
  artyom: {
    avatarUrl: '/assets/author/artyom.jpg',
    name: 'Artyom Kartasov',
    role: 'Software Engineer at',
    twitterUrl: 'https://twitter.com/arkartasov',
    gitlabUrl: 'https://github.com/agneum',
    githubUrl: 'https://gitlab.com/akartasov',
  },
};

export default authors;
